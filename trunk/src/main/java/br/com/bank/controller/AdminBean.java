package br.com.bank.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.inject.Named;

/**
 *
 * @author sr. Gleyson Gama
 */
@Named("admin")
@ViewScoped
public class AdminBean implements Serializable {

    private String event;
    private List<String> events;

    private String album;
    private List<String> albuns;

    private List<BufferedImage> imagens;

    private boolean selectedEvent;
    private boolean selectedAlbum;
    
    private static final String SELECT = "Selecione";

    @PostConstruct
    public void init() {
        events = new ArrayList<>();
        albuns = new ArrayList<>();
        imagens = new ArrayList<>();
        loadEvents();
        setSelectedEvent(false);
        setSelectedAlbum(false);
    }

    public void loadImages() {
        File directory = new File(album);
        if (directory.isDirectory()) {
            for (File img : directory.listFiles()) {
                BufferedImage imagem = null;
                try {
                    imagem = ImageIO.read(new File(img.getAbsolutePath()));
                } catch (IOException ex) {
                    Logger.getLogger(AdminBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                imagens.add(imagem);
                setSelectedAlbum(true);
            }
        }
    }

    public void loadEvents() {
        events.clear();
        events.add(SELECT);
        events.add("Formatura");
        events.add("Show");
        setSelectedAlbum(false);
    }

    public void loadAlbuns() {
        albuns.clear();
        albuns.add(SELECT);
        switch (event) {
            case "Formatura":
                albuns.add("Computacao");
                albuns.add("Medicina");
                albuns.add("Direto");
                setSelectedEvent(true);
                break;
            case "Show":
                albuns.add("Sertanejo");
                albuns.add("Rock");
                albuns.add("Reggae");
                setSelectedEvent(true);
                break;
        }
    }
    
    /**
     * Get's end Set's
     */
    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
    
    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }

    public List<String> getAlbuns() {
        return albuns;
    }

    public void setAlbuns(List<String> albuns) {
        this.albuns = albuns;
    }

    public List<BufferedImage> getImagens() {
        return imagens;
    }

    public void setImagens(List<BufferedImage> imagens) {
        this.imagens = imagens;
    }

    public boolean isSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(boolean selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public boolean isSelectedAlbum() {
        return selectedAlbum;
    }

    public void setSelectedAlbum(boolean selectedAlbum) {
        this.selectedAlbum = selectedAlbum;
    }

    
}
