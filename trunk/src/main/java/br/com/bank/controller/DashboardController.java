package br.com.bank.controller;

import br.com.bank.model.Agencia;
import br.com.bank.model.Cliente;
import br.com.bank.model.ClienteConta;
import br.com.bank.model.Conta;
import br.com.bank.model.ContaCorrente;
import br.com.bank.model.ContaPoupanca;
import br.com.bank.model.Pessoa;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author sr. Gleyson Gama
 */
@Named("dash")
@ViewScoped
public class DashboardController implements Serializable {

    private Pessoa cliente;
    private Conta contaCorrente;
    private Conta contaPoupanca;
    private ClienteConta clienteConta;
    private Agencia agencia;

    public DashboardController() {
        this.cliente = new Cliente();
        this.agencia = new Agencia();
        this.contaCorrente = new ContaCorrente();
        this.contaPoupanca = new ContaPoupanca();
        this.clienteConta = new ClienteConta();
        
        
        
    }

    /****** Get's and Set's ******/
    
    public Pessoa getCliente() {
        return cliente;
    }

    public void setCliente(Pessoa cliente) {
        this.cliente = cliente;
    }

    public Conta getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(Conta contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Conta getContaPoupanca() {
        return contaPoupanca;
    }

    public void setContaPoupanca(Conta contaPoupanca) {
        this.contaPoupanca = contaPoupanca;
    }

    public ClienteConta getClienteConta() {
        return clienteConta;
    }

    public void setClienteConta(ClienteConta clienteConta) {
        this.clienteConta = clienteConta;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }
    
    
}
