package br.com.bank.model;

/**
 *
 * @author gleysongama
 */
public enum TipoTransacao {
    
    CONSULTA_SALDO, TRANSFERENCIA, PAGAMENTO
}
