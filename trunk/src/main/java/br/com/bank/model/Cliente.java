package br.com.bank.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gleysongama
 */
public class Cliente extends Pessoa {

    private String codigo;
    private Agencia agenciaBancaria;
    private List<ClienteConta> listClienteConta;

    public Cliente() {
        listClienteConta = new ArrayList<>();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Agencia getAgenciaBancaria() {
        return agenciaBancaria;
    }

    public void setAgenciaBancaria(Agencia agenciaBancaria) {
        this.agenciaBancaria = agenciaBancaria;
    }

    public List<ClienteConta> getListClienteConta() {
        return listClienteConta;
    }

    public void setListClienteConta(List<ClienteConta> listClienteConta) {
        this.listClienteConta = listClienteConta;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.codigo);
        hash = 19 * hash + Objects.hashCode(this.agenciaBancaria);
        hash = 19 * hash + Objects.hashCode(this.listClienteConta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.agenciaBancaria, other.agenciaBancaria)) {
            return false;
        }
        if (!Objects.equals(this.listClienteConta, other.listClienteConta)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cliente{" + "codigo=" + codigo + ", agenciaBancaria=" + agenciaBancaria + ", listClienteConta=" + listClienteConta + '}';
    }
}
