package br.com.bank.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gleysongama
 */
public class ClienteConta {
    
    private Cliente cliente;
    private Conta conta;
    private Date dateAbertura;
    private Date dateEncerramento;
    private boolean status;
    private List<Transacao> transacao;

    public ClienteConta() {
        this.transacao = new ArrayList<>();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public Date getDateAbertura() {
        return dateAbertura;
    }

    public void setDateAbertura(Date dateAbertura) {
        this.dateAbertura = dateAbertura;
    }

    public Date getDateEncerramento() {
        return dateEncerramento;
    }

    public void setDateEncerramento(Date dateEncerramento) {
        this.dateEncerramento = dateEncerramento;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Transacao> getTransacao() {
        return transacao;
    }

    public void setTransacao(List<Transacao> transacao) {
        this.transacao = transacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.cliente);
        hash = 37 * hash + Objects.hashCode(this.conta);
        hash = 37 * hash + Objects.hashCode(this.dateAbertura);
        hash = 37 * hash + Objects.hashCode(this.dateEncerramento);
        hash = 37 * hash + (this.status ? 1 : 0);
        hash = 37 * hash + Objects.hashCode(this.transacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteConta other = (ClienteConta) obj;
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.conta, other.conta)) {
            return false;
        }
        if (!Objects.equals(this.dateAbertura, other.dateAbertura)) {
            return false;
        }
        if (!Objects.equals(this.dateEncerramento, other.dateEncerramento)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.transacao, other.transacao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClienteConta{" + "cliente=" + cliente + ", conta=" + conta + ", dateAbertura=" + dateAbertura + ", dateEncerramento=" + dateEncerramento + ", status=" + status + ", transacaos=" + transacao + '}';
    }
}
