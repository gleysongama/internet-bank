package br.com.bank.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gleysongama
 */
public class Bancario extends Pessoa {

    private String matricula;
    private Agencia agenciaBancaria;
    private List<BancarioConta> listBancarioConta;
    
    public Bancario() {
        this.listBancarioConta = new ArrayList<>();
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Agencia getAgenciaBancaria() {
        return agenciaBancaria;
    }

    public void setAgenciaBancaria(Agencia agenciaBancaria) {
        this.agenciaBancaria = agenciaBancaria;
    }

    public List<BancarioConta> getListBancarioConta() {
        return listBancarioConta;
    }

    public void setListBancarioConta(List<BancarioConta> listBancarioConta) {
        this.listBancarioConta = listBancarioConta;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.matricula);
        hash = 53 * hash + Objects.hashCode(this.agenciaBancaria);
        hash = 53 * hash + Objects.hashCode(this.listBancarioConta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bancario other = (Bancario) obj;
        if (!Objects.equals(this.matricula, other.matricula)) {
            return false;
        }
        if (!Objects.equals(this.agenciaBancaria, other.agenciaBancaria)) {
            return false;
        }
        if (!Objects.equals(this.listBancarioConta, other.listBancarioConta)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bancario{" + "matricula=" + matricula + ", agenciaBancaria=" + agenciaBancaria + ", listBancarioConta=" + listBancarioConta + '}';
    }
}
