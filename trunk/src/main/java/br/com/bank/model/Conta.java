package br.com.bank.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gleysongama
 */
public abstract class Conta {
    
    private int numero;
    private double saldo;
    private double taxaManutencao;
    private boolean status;
    private List<ClienteConta> listClienteConta;
    private List<BancarioConta> listBancarioConta;

    public Conta() {
        this.listClienteConta = new ArrayList<>();
        this.listBancarioConta = new ArrayList<>();
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getTaxaManutencao() {
        return taxaManutencao;
    }

    public void setTaxaManutencao(double taxaManutencao) {
        this.taxaManutencao = taxaManutencao;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<ClienteConta> getListClienteConta() {
        return listClienteConta;
    }

    public void setListClienteConta(List<ClienteConta> listClienteConta) {
        this.listClienteConta = listClienteConta;
    }

    public List<BancarioConta> getListBancarioConta() {
        return listBancarioConta;
    }

    public void setListBancarioConta(List<BancarioConta> listBancarioConta) {
        this.listBancarioConta = listBancarioConta;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.numero;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.saldo) ^ (Double.doubleToLongBits(this.saldo) >>> 32));
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.taxaManutencao) ^ (Double.doubleToLongBits(this.taxaManutencao) >>> 32));
        hash = 59 * hash + (this.status ? 1 : 0);
        hash = 59 * hash + Objects.hashCode(this.listClienteConta);
        hash = 59 * hash + Objects.hashCode(this.listBancarioConta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conta other = (Conta) obj;
        if (this.numero != other.numero) {
            return false;
        }
        if (Double.doubleToLongBits(this.saldo) != Double.doubleToLongBits(other.saldo)) {
            return false;
        }
        if (Double.doubleToLongBits(this.taxaManutencao) != Double.doubleToLongBits(other.taxaManutencao)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.listClienteConta, other.listClienteConta)) {
            return false;
        }
        if (!Objects.equals(this.listBancarioConta, other.listBancarioConta)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Conta{" + "numero=" + numero + ", saldo=" + saldo + ", taxaManutencao=" + taxaManutencao + ", status=" + status + ", listClienteConta=" + listClienteConta + ", listBancarioConta=" + listBancarioConta + '}';
    }
}
