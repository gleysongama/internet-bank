package br.com.bank.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gleysongama
 */
public class Agencia {
    
    private int numero;
    private Endereco endereco;
    private List<Cliente> listClientes;
    private List<Bancario> listBancarios;
    private List<Conta> listContas;

    public Agencia() {
        this.listBancarios = new ArrayList<>();
        this.listClientes = new ArrayList<>();
        this.listContas = new ArrayList<>();
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Cliente> getListClientes() {
        return listClientes;
    }

    public void setListClientes(List<Cliente> listClientes) {
        this.listClientes = listClientes;
    }

    public List<Bancario> getListBancarios() {
        return listBancarios;
    }

    public void setListBancarios(List<Bancario> listBancarios) {
        this.listBancarios = listBancarios;
    }

    public List<Conta> getListContas() {
        return listContas;
    }

    public void setListContas(List<Conta> listContas) {
        this.listContas = listContas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.numero;
        hash = 97 * hash + Objects.hashCode(this.endereco);
        hash = 97 * hash + Objects.hashCode(this.listClientes);
        hash = 97 * hash + Objects.hashCode(this.listBancarios);
        hash = 97 * hash + Objects.hashCode(this.listContas);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agencia other = (Agencia) obj;
        if (this.numero != other.numero) {
            return false;
        }
        if (!Objects.equals(this.endereco, other.endereco)) {
            return false;
        }
        if (!Objects.equals(this.listClientes, other.listClientes)) {
            return false;
        }
        if (!Objects.equals(this.listBancarios, other.listBancarios)) {
            return false;
        }
        if (!Objects.equals(this.listContas, other.listContas)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AgenciaBancaria{" + "numero=" + numero + ", endereco=" + endereco + ", listClientes=" + listClientes + ", listBancarios=" + listBancarios + ", listContas=" + listContas + '}';
    }
    
}
