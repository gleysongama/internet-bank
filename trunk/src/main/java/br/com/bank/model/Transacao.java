package br.com.bank.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author gleysongama
 */
public class Transacao {
    
    private Date dateTransacao;
    private ClienteConta clienteConta;
    private TipoTransacao tipoTransacao;

    public Transacao() {
        this.dateTransacao = new Date();
    }

    public Date getDateTransacao() {
        return dateTransacao;
    }

    public void setDateTransacao(Date dateTransacao) {
        this.dateTransacao = dateTransacao;
    }

    public ClienteConta getClienteConta() {
        return clienteConta;
    }

    public void setClienteConta(ClienteConta clienteConta) {
        this.clienteConta = clienteConta;
    }

    public TipoTransacao getTipoTransacao() {
        return tipoTransacao;
    }

    public void setTipoTransacao(TipoTransacao tipoTransacao) {
        this.tipoTransacao = tipoTransacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.dateTransacao);
        hash = 29 * hash + Objects.hashCode(this.clienteConta);
        hash = 29 * hash + Objects.hashCode(this.tipoTransacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transacao other = (Transacao) obj;
        if (!Objects.equals(this.dateTransacao, other.dateTransacao)) {
            return false;
        }
        if (!Objects.equals(this.clienteConta, other.clienteConta)) {
            return false;
        }
        if (this.tipoTransacao != other.tipoTransacao) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Transacao{" + "dateTransacao=" + dateTransacao + ", clienteConta=" + clienteConta + ", tipoTransacao=" + tipoTransacao + '}';
    }
}
