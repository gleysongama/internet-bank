package br.com.bank.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gleysongama
 */
public class Endereco {
    
    private String logradouro;
    private int numero;
    private String cep;
    private String pais;
    private String estado;
    private String cidade;
    private List<Pessoa> listPessoa;

    public Endereco() {
        this.listPessoa = new ArrayList<>();
    }
    
    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.logradouro);
        hash = 13 * hash + this.numero;
        hash = 13 * hash + Objects.hashCode(this.cep);
        hash = 13 * hash + Objects.hashCode(this.pais);
        hash = 13 * hash + Objects.hashCode(this.estado);
        hash = 13 * hash + Objects.hashCode(this.cidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Endereco other = (Endereco) obj;
        if (!Objects.equals(this.logradouro, other.logradouro)) {
            return false;
        }
        if (this.numero != other.numero) {
            return false;
        }
        if (!Objects.equals(this.cep, other.cep)) {
            return false;
        }
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Endereco{" + "logradouro=" + logradouro + ", numero=" + numero + ", cep=" + cep + ", pais=" + pais + ", estado=" + estado + ", cidade=" + cidade + '}';
    }
}
