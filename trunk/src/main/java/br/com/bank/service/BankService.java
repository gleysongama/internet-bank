package br.com.bank.service;

import br.com.bank.model.Cliente;
import br.com.bank.model.Conta;
import br.com.bank.model.Pessoa;
import br.com.bank.model.Transacao;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sr. Gleyson Gama
 */
public class BankService {
    
    public Cliente novoCliente() {
        Cliente cliente = new Cliente();
        cliente.setNome("Gleyson do Nascimento Gama");
        cliente.setCpf("93359527291");
        cliente.setRg("4892962");
        cliente.setFone("91991448739");
        cliente.setProfissao("Arquiteto de Software");
        cliente.setPai("Antonio Lima Gama");
        cliente.setMae("Maria Idemarina do Nascimento");
        cliente.setNascimento(new Date(1987, 06, 27));
        return cliente;
    }
    
    public Conta novaConta() {
        return null;
    }
    
    public double cosultarSaldoConta(Cliente cliente, Conta conta) {
        return 0;
    }
    
    public List<Transacao> consultarExtrato(Cliente cliente, Conta conta) {
        return null;
    }
    
    public boolean realizarPagamento(Cliente cliente, Conta conta, double valor) {
        return false;
    }
    
    public boolean realizarTransferencia(Cliente cliente, Conta contaOrigem, Conta contaDestino, double valor) {
        return false;
    }
}
