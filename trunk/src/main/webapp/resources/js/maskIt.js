/**
 * Função para aplicar máscara em campos de texto
 * Copyright (c) 2008, Dirceu Bimonti Ivo - http://www.bimonti.net
 * All rights reserved.
 * @constructor
 */

/* Version 0.27 */

/**
 * Função Principal
 * @param w - O elemento que será aplicado (normalmente this).
 * @param e - O evento para capturar a tecla e cancelar o backspace.
 * @param m - A máscara a ser aplicada.
 * @param r - Se a máscara deve ser aplicada da direita para a esquerda. Veja Exemplos.
 * @param a -
 * @param p - true, deve aceitar letras
 * @returns null
 
 * @example - 
 *  cpf -       onkeyup="maskIt(this,event,'###.###.###-##')"
 *  fone -      onkeyup="maskIt(this,event,'(##)####-####')"
 *  celular -   onkeyup="maskCel(this)"
 *  dinheiro -  onkeyup="maskIt(this,event,'###.###.###,##',true,{pre:'R$', pos:''})"
 *  graus -     onkeyup="maskIt(this,event,'###,#',true,{pre:'',pos:'º'})"
 *  placa -     onkeyup="maskIt(this,event,'&&&-####')" & = Carácter não numérico # = Carácter numérico
 */

function maskIt(w, m, r, a, p) {
    // Variáveis da função
    if (p === true) {
        var txt = (!r) ? w.value.replace(/[^\w]+/gi, '') : w.value.replace(/[^\w]+/gi, '').reverse();
    } else {
        var txt = (!r) ? w.value.replace(/[^\d]+/gi, '') : w.value.replace(/[^\d]+/gi, '').reverse();
    }
    var mask = (!r) ? m : m.reverse();
    var pre = (a) ? a.pre : "";
    var pos = (a) ? a.pos : "";
    var ret = "";

    // Loop na máscara para aplicar os caracteres
    for (var x = 0, y = 0, z = mask.length; x < z && y < txt.length; ) {
        if ((mask.charAt(x) != '#') && (mask.charAt(x) != '&')) {
            ret += mask.charAt(x);
            x++;
        } else {
            if (mask.charAt(x) == '#') {
                ret += txt.charAt(y).replace(/[^\d]+/gi, '');
                y++;
                x++;
            } else if (mask.charAt(x) == '&') {
                ret += txt.charAt(y).replace(/[\d]+/gi, '');
                y++;
                x++;
            }
        }
    }

    // Retorno da função
    ret = (!r) ? ret : ret.reverse()
    w.value = pre + ret + pos;
}

// Novo método para o objeto 'String'
String.prototype.reverse = function () {
    return this.split('').reverse().join('');
};

function maskCel(obj) {
    var str = obj.value;
    var ddd = str.substring(1, 3);
    var pdi = str.substring(5, 6);
    if ((ddd == 91) && (pdi == 9)) {
        maskIt(obj, '(##) # ####-####', false, "", true);
    } else {
        maskIt(obj, '(##) ####-####', false, "", true);
    }
}